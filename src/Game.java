import Grid.Ball;
import Grid.BallsGrid;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.io.IOException;
import java.util.ArrayList;

public class Game {

    Cannon myCannon;
    boolean gameOver;
    BallsGrid myGrid;

    Player player;
    AnimationGen animationGen;
    GameSaver gameSaver;


    public void init() {
        animationGen = new AnimationGen();
        animationGen.initBackGround();

        myGrid = new BallsGrid();
        myGrid.init(7);

        player = new Player();
        myCannon = new Cannon(myGrid.getBallArrayList());
        // myCannon.shoot(0, 750);

        try {
            gameSaver = new GameSaver();
            animationGen.setGameSaver(gameSaver);
            GameSaver.displayCurrentScore();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MusicGen.music();

    }



}
