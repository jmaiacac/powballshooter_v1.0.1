import sun.audio.AudioData;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import sun.audio.*;


public class MusicGen {


    private static boolean playBackGround = true;

    static AudioPlayer MGP = AudioPlayer.player;
    static AudioStream BGM;
    static AudioData MD;

    static InputStream background;


    static ContinuousAudioDataStream loop = null;


        public static void music()
        {

            try
            {
                background = new FileInputStream("powpowMusic.wav");
                BGM = new AudioStream(background);
                AudioPlayer.player.start(BGM);
                MD = BGM.getData();
                loop = new ContinuousAudioDataStream(MD);

            }
            catch(FileNotFoundException e){
                System.out.print(e.toString());
            }
            catch(IOException error)
            {
                System.out.print(error.toString());
            }
            MGP.start(loop);
        }

        public static void muteBackGround() throws IOException {
            MusicGen.background.close();
            playBackGround = false;
            AnimationGen.soundButton.load("buttonIsMuted.png");
            AnimationGen.soundButton.draw();

        }

        public static void playBackGround() throws IOException {
            playBackGround = true;
            music();
            AnimationGen.soundButton.load("audiobutton.png");
            AnimationGen.soundButton.draw();
        }

    public static void ballHit()
    {
        AudioPlayer MGP2 = AudioPlayer.player;
        AudioStream BGM2;
        //AudioData MD;

        ContinuousAudioDataStream loop2 = null;

        try
        {
            InputStream test2 = new FileInputStream("ballhit.wav");
            BGM2 = new AudioStream(test2);
            AudioPlayer.player.start(BGM2);

            //MD = BGM.getData();
            //loop = new ContinuousAudioDataStream(MD);
        }
        catch(FileNotFoundException e){
            System.out.print(e.toString());
        }
        catch(IOException error)
        {
            System.out.print(error.toString());
        }
        MGP.start(loop2);
    }

    public static void multipleBallHit()
    {
        AudioPlayer MGP3 = AudioPlayer.player;
        AudioStream BGM3;
        //AudioData MD;

        ContinuousAudioDataStream loop3 = null;

        try
        {
            InputStream test = new FileInputStream("multipleballhit.wav");
            BGM3 = new AudioStream(test);
            AudioPlayer.player.start(BGM3);
            //MD = BGM.getData();
            //loop = new ContinuousAudioDataStream(MD);

        }
        catch(FileNotFoundException e){
            System.out.print(e.toString());
        }
        catch(IOException error)
        {
            System.out.print(error.toString());
        }
        MGP.start(loop);
    }
    public static boolean isPlayBackGround() {
        return playBackGround;
    }

    public static void setPlayBackGround(boolean playBackGround) {
        MusicGen.playBackGround = playBackGround;
    }


    }

